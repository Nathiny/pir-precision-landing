#!/usr/bin/python

import numpy as np
import matplotlib.pyplot as plt

def read_command(filename):
    file=open(filename,"r")
    roll_command, pitch_command, climb_command, phi_real, theta_real, psi_real, posx, posy, posz = [], [], [], [], [], [], [], [], []
    dronex, droney, dronez, predx, predy, predz, pltx, plty, pltz = [], [], [], [], [], [], [], [], []
    ts = []
    i = 0
    for row in file :
        i += 1
        if i > 1:
            ts.append(i)
            line=row.split(";")
            roll_command.append(float(line[0]))
            pitch_command.append(float(line[1]))
            climb_command.append(float(line[2]))
            phi_real.append(float(line[3]))
            theta_real.append(float(line[4]))
            psi_real.append(float(line[5]))
            posx.append(float(line[6]))
            posy.append(float(line[7]))
            posz.append(float(line[8]))
            dronex.append(float(line[9]))
            droney.append(float(line[10]))
            dronez.append(float(line[11]))
            predx.append(float(line[12]))
            predy.append(float(line[13]))
            predz.append(float(line[14]))
            pltx.append(float(line[15]))
            plty.append(float(line[16]))
            pltz.append(float(line[17]))
    max_roll_command = max(abs(np.array(roll_command)))
    max_pitch_command = max(abs(np.array(pitch_command)))
    max_phi_real = max(abs(np.array(phi_real)))
    max_theta_real = max(abs(np.array(theta_real)))
    max_posx = max(abs(np.array(posx)))
    max_posy = max(abs(np.array(posy)))
    max_posz = max(abs(np.array(posz)))
    max_dronex = max(abs(np.array(dronex)))
    max_droney = max(abs(np.array(droney)))
    max_dronez = max(abs(np.array(dronez)))
    max_predx = max(abs(np.array(predx)))
    max_predy = max(abs(np.array(predy)))
    max_predz = max(abs(np.array(predz)))
    max_pltx = max(abs(np.array(pltx)))
    max_plty = max(abs(np.array(plty)))
    max_pltz = max(abs(np.array(pltz)))
    for i in range(len(roll_command)):
        roll_command[i] = roll_command[i]/max_roll_command
        pitch_command[i] = pitch_command[i]/max_pitch_command
        #climb_command[i] = roll_command[i]/max_roll_command
        phi_real[i] = phi_real[i]/max_phi_real
        theta_real[i] = roll_command[i]/max_theta_real
        # posx[i] = -posx[i]/max_posx
        # posy[i] = posy[i]/max_posy
        # dronex[i] = dronex[i]/max_dronex
        # droney[i] = droney[i]/max_droney
        # predx[i] = predx[i]/max_predx
        # predy[i] = predy[i]/max_predy
        # pltx[i] = pltx[i]/max_pltx
        # plty[i] = plty[i]/max_plty
        posx[i] = -posx[i]
        posy[i] = posy[i]
        posz[i] = posz[i]
        dronex[i] = dronex[i]*1000
        droney[i] = droney[i] * 1000
        dronez[i] = dronez[i]*1000
        predx[i] = predx[i]*1000
        predy[i] = predy[i] * 1000
        predz[i] = predz[i]*1000
        pltx[i] = pltx[i]*1000
        plty[i] = plty[i] * 1000
        pltz[i] = pltz[i]*1000

    file.close()
    return roll_command, pitch_command, phi_real, theta_real, posx, posy, posz, dronex, droney, dronez, predx, predy, predz, pltx, plty, pltz, ts

def read_command2D(filename):
    file=open(filename,"r")
    # roll_command, pitch_command, climb_command, phi_real, theta_real, psi_real, posx, posy, = [], [], [], [], [], [], [], []
    # dronex, droney, predx, predy, pltx, plty, = [], [], [], [], [], []
    roll_command, pitch_command, climb_command, phi_real, theta_real, psi_real, posx, posy = [], [], [], [], [], [], [], []
    dronex, droney, predx, predy, pltx, plty = [], [], [], [], [], []
    ts = []
    i = 0
    for row in file :
        i += 1
        if i > 1:
            ts.append(i)
            line=row.split(";")
            roll_command.append(float(line[0]))
            pitch_command.append(float(line[1]))
            climb_command.append(float(line[2]))
            phi_real.append(float(line[3]))
            theta_real.append(float(line[4]))
            psi_real.append(float(line[5]))
            posx.append(float(line[6]))
            posy.append(float(line[7]))
            dronex.append(float(line[8]))
            droney.append(float(line[9]))
            predx.append(float(line[10]))
            predy.append(float(line[11]))
            pltx.append(float(line[12]))
            plty.append(float(line[13]))
    max_roll_command = max(abs(np.array(roll_command)))
    max_pitch_command = max(abs(np.array(pitch_command)))
    max_phi_real = max(abs(np.array(phi_real)))
    max_theta_real = max(abs(np.array(theta_real)))
    max_posx = max(abs(np.array(posx)))
    max_posy = max(abs(np.array(posy)))
    max_dronex = max(abs(np.array(dronex)))
    max_droney = max(abs(np.array(droney)))
    max_predx = max(abs(np.array(predx)))
    max_predy = max(abs(np.array(predy)))
    max_pltx = max(abs(np.array(pltx)))
    max_plty = max(abs(np.array(plty)))
    for i in range(len(roll_command)):
        roll_command[i] = roll_command[i]/max_roll_command
        pitch_command[i] = pitch_command[i]/max_pitch_command
        phi_real[i] = phi_real[i]/max_phi_real
        theta_real[i] = roll_command[i]/max_theta_real
        posx[i] = -posx[i]
        posy[i] = posy[i]
        dronex[i] = dronex[i]*1000
        droney[i] = droney[i] * 1000
        predx[i] = predx[i]*1000
        predy[i] = predy[i] * 1000
        pltx[i] = pltx[i]*1000
        plty[i] = plty[i] * 1000
    file.close()
    return roll_command, pitch_command, phi_real, theta_real, posx, posy, dronex, droney, predx, predy, pltx, plty, ts


def trace(roll_command, pitch_command, phi_real, theta_real, posx, posy, posz, dronex, droney, dronez, predx, predy, predz, pltx, plty, pltz, ts):
    fig, axs = plt.subplots(3, figsize=(15, 8), dpi=150)
    fig.suptitle("Positions du drone (OptiTrack), de la plateforme prédite par le filtre de Kalman\net de la plateforme réelle (OptiTrack)")
    axs[0].plot(ts, dronex, '+b', label='drone')
    axs[0].plot(ts, predx, '+y', label='prédiction')
    axs[0].plot(ts, pltx, '+r', label='plateforme')
    # axs[0].plot(ts, roll_command,  '.b', label='roll command')
    # axs[0].plot(ts, phi_real, '+r', label='roll real')
    # axs[0].plot(ts, posy, '+r', label='posy')
    axs[1].plot(ts, droney, '+b', label='drone')
    axs[1].plot(ts, predy, '+y', label='prédiction')
    axs[1].plot(ts, plty, '+r', label='plateforme')
    # axs[1].plot(ts, pitch_command, '.b', label='pitch command')
    # axs[1].plot(ts, theta_real, '+r', label='pitch real')
    # axs[1].plot(ts, posx, '+r', label='posx')
    axs[2].plot(ts, dronez, '+b', label='drone')
    axs[2].plot(ts, predz, '+y', label='prédiction')
    axs[2].plot(ts, pltz, '+r', label='plateforme')
    for i in range(3):
        axs[i].legend()
        axs[i].set_xlabel("temps (s)")
    axs[0].set_ylabel("x (mm)")
    axs[1].set_ylabel("y (mm)")
    axs[2].set_ylabel("z (mm)")
    plt.show()

def trace2D(roll_command, pitch_command, phi_real, theta_real, posx, posy, dronex, droney, predx, predy, pltx, plty, ts):
    fig, axs = plt.subplots(2, figsize=(15, 8), dpi=150)
    fig.suptitle("Positions du drone (OptiTrack), de la plateforme prédite par le filtre de Kalman\net de la plateforme réelle (OptiTrack)")
    axs[0].plot(ts, dronex, '+b', label='drone')
    axs[0].plot(ts, predx, '+y', label='prédiction')
    axs[0].plot(ts, pltx, '+r', label='plateforme')

    axs[1].plot(ts, droney, '+b', label='drone')
    axs[1].plot(ts, predy, '+y', label='prédiction')
    axs[1].plot(ts, plty, '+r', label='plateforme')

    for i in range(2):
        axs[i].legend()
        axs[i].set_xlabel("temps (s)")
    axs[0].set_ylabel("x (mm)")
    axs[1].set_ylabel("y (mm)")
    plt.show()

def trace_ecart(roll_command, pitch_command, phi_real, theta_real, posx, posy, posz, dronex, droney, dronez, predx, predy, predz, pltx, plty, pltz, ts):
    fig, axs = plt.subplots(3, figsize=(15, 8), dpi=150)
    fig.suptitle("Écarts entre la position du drone (OptiTrack) et la position de la plateforme prédite par le filtre de Kalman, \net entre la position prédite et la position réelle de la plateforme (OptiTrack)", fontsize=16)
    axs[0].plot(ts, np.array(dronex) - np.array(predx), '+b', label='drone / prédiction')
    axs[0].plot(ts, np.array(predx) - np.array(pltx), '+r', label='prédiction / plateforme')
    axs[0].plot(ts, np.array(dronex) - np.array(pltx), '+y', label='drone / plateforme')
    axs[1].plot(ts, np.array(droney) - np.array(predy), '+b', label='drone / prédiction')
    axs[1].plot(ts, np.array(predy) - np.array(plty), '+r', label='prédiction / plateforme')
    axs[1].plot(ts, np.array(droney) - np.array(plty), '+y', label='drone / plateforme')
    axs[2].plot(ts, np.array(dronez) - np.array(predz), '+b', label='drone / prédiction')
    axs[2].plot(ts, np.array(predz) - np.array(pltz), '+r', label='prédiction / plateforme')
    axs[2].plot(ts, np.array(dronez) - np.array(pltz), '+y', label='drone / plateforme')
    for i in range(3):
        axs[i].legend()
        axs[i].set_xlabel("temps (s)")
    axs[0].set_ylabel("x (mm)")
    axs[1].set_ylabel("y (mm)")
    axs[2].set_ylabel("z (mm)")
    plt.show()

def trace_ecart2D(roll_command, pitch_command, phi_real, theta_real, posx, posy, dronex, droney, predx, predy, pltx, plty, ts):
    fig, axs = plt.subplots(2, figsize=(15, 8), dpi=150)
    fig.suptitle("Écarts entre la position du drone (OptiTrack) et la position de la plateforme prédite par le filtre de Kalman, \net entre la position prédite et la position réelle de la plateforme (OptiTrack)", fontsize=16)
    axs[0].plot(ts, np.array(dronex) - np.array(predx), '+b', label='drone / prédiction')
    axs[0].plot(ts, np.array(predx) - np.array(pltx), '+r', label='prédiction / plateforme')
    axs[0].plot(ts, np.array(dronex) - np.array(pltx), '+y', label='drone / plateforme')
    axs[1].plot(ts, np.array(droney) - np.array(predy), '+b', label='drone / prédiction')
    axs[1].plot(ts, np.array(predy) - np.array(plty), '+r', label='prédiction / plateforme')
    axs[1].plot(ts, np.array(droney) - np.array(plty), '+y', label='drone / plateforme')
    for i in range(2):
        axs[i].legend()
        axs[i].set_xlabel("temps (s)")
    axs[0].set_ylabel("x (mm)")
    axs[1].set_ylabel("y (mm)")
    plt.show()

# roll_command, pitch_command, phi_real, theta_real, posx, posy, posz, dronex, droney, dronez, predx, predy, predz, pltx, plty, pltz,  ts = read_command("kalman_command_d6")
# trace(roll_command, pitch_command, phi_real, theta_real, posx, posy, posz, dronex, droney, dronez, predx, predy, predz, pltx, plty, pltz, ts)
# trace_ecart(roll_command, pitch_command, phi_real, theta_real, posx, posy, posz, dronex, droney, dronez, predx, predy, predz, pltx, plty, pltz, ts)

roll_command, pitch_command, phi_real, theta_real, posx, posy, dronex, droney, predx, predy, pltx, plty, ts = read_command2D("kalman_command5")
trace2D(roll_command, pitch_command, phi_real, theta_real, posx, posy, dronex, droney, predx, predy, pltx, plty, ts)
trace_ecart2D(roll_command, pitch_command, phi_real, theta_real, posx, posy, dronex, droney, predx, predy, pltx, plty, ts)
