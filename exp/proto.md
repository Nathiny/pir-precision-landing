% **Protocole expérimental**

---
documentclass: extarticle
numbersections: false
fontsize: 12pt
margin-left: 2.5cm
margin-right: 2.5cm
margin-top: 2cm
margin-bottom: 2cm
header-includes: |
    \usepackage{fancyhdr}
    \pagestyle{fancy}
    \fancyhead[L]{}
    \fancyhead[R]{\textsc{Protocole expérimental}}
    \fancyfoot[CE,CO]{\thepage}
    \renewcommand{\headrulewidth}{1pt}
    \renewcommand{\footrulewidth}{1pt}
    \usepackage{gensymb}
---

- **Informations à récupérer :**
  - positions et orientations du drone et de la plateforme dans le référentiel terrestre
  - positions et orientations de la plateforme dans le référentiel de la JeVois


- **Trajectoires à réaliser :**
  - linéaires perpendiculaires à vitesses constantes
  - linéaires perpendiculaires à vitesse constante pour la plateforme et accélération constante pour le drone
  - linéaires parallèles à vitesses constantes différentes dans le même sens
  - linéaires sécantes avec un angle de 20° à vitesses constantes identiques
  - circulaire pour la plateforme et linéaire pour le drone à vitesses constantes
  - circulaire pour le drone et linéaire pour la plateforme à vitesses constantes