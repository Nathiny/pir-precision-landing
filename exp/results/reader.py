import numpy as np

def convert_to_nparray(xarray):
    return np.array([np.array(x) for x in xarray])
   

def read_log(filename, eps = 2):
    file=open(filename,"r")
    x_drone, y_drone, z_drone = [],[],[]
    x_drone_img, y_drone_img, z_drone_img = [],[],[]
    t_img = []
    quaternions = []
    xc, yc, zc = [], [], []
    i = 0
    s = 0
    q = 16
    for row in file :
        i += 1
        line=row.split(" ")
        if i==2 :
            x_drone_img.append([float(line[13])])
            y_drone_img.append([float(line[14])])
            z_drone_img.append([float(line[15])])
            xc.append([float(line[3])])
            yc.append([float(line[4])])
            zc.append([float(line[5])])
            t_img.append([float(line[-1])/1000])
            quaternions.append([np.array([float(line[q+1]), float(line[q+2]), float(line[q+3]), float(line[q])])]) #quaternion form : [qx, qy, qz, qi]
        if i>2 :
            if line[1] != 'None':
                t = float(line[-1])/1000
                if t - t_img[s][-1] > eps:
                    s += 1
                    x_drone_img.append([float(line[13])])
                    y_drone_img.append([float(line[14])])
                    z_drone_img.append([float(line[15])])
                    t_img.append([float(line[-1])/1000])
                    xc.append([float(line[3])])
                    yc.append([float(line[4])])
                    zc.append([float(line[5])])
                    quaternions.append([np.array([float(line[q+1]), float(line[q+2]), float(line[q+3]), float(line[q])])])
                elif t != t_img[s][-1] :
                    x_drone_img[s].append(float(line[13]))
                    y_drone_img[s].append(float(line[14]))
                    z_drone_img[s].append(float(line[15]))
                    t_img[s].append(float(line[-1])/1000)
                    x, y, z = float(line[3]), float(line[4]), float(line[5])
                    xc[s].append(x)
                    yc[s].append(y)
                    zc[s].append(z)
                    quaternions[s].append(np.array([float(line[q+1]), float(line[q+2]), float(line[q+3]), float(line[q])]))

    file.close()

    x_drone_img = convert_to_nparray(x_drone_img)
    y_drone_img = convert_to_nparray(y_drone_img)
    z_drone_img = convert_to_nparray(z_drone_img)

    xc = convert_to_nparray(xc)
    yc = convert_to_nparray(yc)
    zc = convert_to_nparray(zc)

    t_img = convert_to_nparray(t_img)

    return t_img, x_drone_img, y_drone_img, z_drone_img, xc, yc, zc, quaternions



filename_measures = "exp_01_measures"

def read_measures_old(filename, t_img, dt=2):
    file=open(filename,"r")
    x_drone_218, y_drone_218, z_drone_218 = [[]],[[]],[[]]
    x_pl, y_pl, z_pl = [[]], [[]], [[]]
    quat = [[]]
    ts = [[]]
    i = 0
    s = 0
    for row in file :
        i += 1
        line=row.split()
        t = float(line[-1])
        if s < len(t_img) and t >= t_img[s][0]-dt and t <= t_img[s][-1]+dt :
            if line[0] == '218':
                x_drone_218[s].append(float(line[1]))
                y_drone_218[s].append(float(line[2]))
                z_drone_218[s].append(3.)
                quat[s].append(np.array([float(line[3+1]), float(line[3+2]), float(line[3+3]), float(line[3])]))
                ts[s].append(t)
            if line[0] == '51':
                x_pl[s].append(float(line[1]))
                y_pl[s].append(float(line[2]))
                z_pl[s].append(0.)
        if s < len(t_img) and t > t_img[s][-1]+dt:
            s += 1
            x_drone_218.append([])
            y_drone_218.append([])
            z_drone_218.append([])
            quat.append([])
            ts.append([])
            x_pl.append([])
            y_pl.append([])
            z_pl.append([])
    
    x_drone_218 = convert_to_nparray(x_drone_218)
    y_drone_218 = convert_to_nparray(y_drone_218)
    z_drone_218 = convert_to_nparray(z_drone_218)

    x_pl = convert_to_nparray(x_pl)
    y_pl = convert_to_nparray(y_pl)
    z_pl = convert_to_nparray(z_pl)

    return ts, x_drone_218, y_drone_218, z_drone_218, quat, x_pl, y_pl, z_pl

def read_measures(filename, t_img, dt=2):
    file=open(filename,"r")
    x_drone_218, y_drone_218, z_drone_218 = [[]],[[]],[[]]
    x_pl, y_pl, z_pl = [[]], [[]], [[]]
    quat = [[]]
    ts = [[]]
    row = 0
    s = 0
    lines = file.readlines()
    # for row in range(len(lines)/2) :
    while (row<len(lines)):
        line_218=lines[row].split()
        line_51=lines[row+1].split()
        if line_218[0]!="218" or line_51[0]!="51":
            row +=1
        else:
            t = float(line_218[-1])
            if s < len(t_img) and t >= t_img[s][0]-dt and t <= t_img[s][-1]+dt :
                x_drone_218[s].append(float(line_218[1]))
                y_drone_218[s].append(float(line_218[2]))
                z_drone_218[s].append(3.)
                quat[s].append(np.array([float(line_218[3+1]), float(line_218[3+2]), float(line_218[3+3]), float(line_218[3])]))
                ts[s].append(t)
                x_pl[s].append(float(line_51[1]))
                y_pl[s].append(float(line_51[2]))
                z_pl[s].append(0.)
            if s < len(t_img) and t > t_img[s][-1]+dt:
                s += 1
                x_drone_218.append([])
                y_drone_218.append([])
                z_drone_218.append([])
                quat.append([])
                ts.append([])
                x_pl.append([])
                y_pl.append([])
                z_pl.append([])
            row += 2
    
    x_drone_218 = convert_to_nparray(x_drone_218)
    y_drone_218 = convert_to_nparray(y_drone_218)
    z_drone_218 = convert_to_nparray(z_drone_218)

    x_pl = convert_to_nparray(x_pl)
    y_pl = convert_to_nparray(y_pl)
    z_pl = convert_to_nparray(z_pl)

    return ts, x_drone_218, y_drone_218, z_drone_218, quat, x_pl, y_pl, z_pl