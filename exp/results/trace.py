import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
from scipy.spatial.transform import Rotation as R
import numpy as np


def platform_in_cam(xc, yc, zc, t_img):
    for session in range(len(xc)):
        xs = xc[session]
        ys = yc[session]
        zs = zc[session]
        ts = t_img[session]

        fig, ax = plt.subplots()

        ax.scatter(xs, ys)

        for i in range(len(xs)):
            ax.annotate(i, (xs[i], ys[i]))

        ax.axis('equal')
        plt.show()


def platform_in_real_world(xo, yo, zo, xc, yc, zc, xd, yd, t):
    '''xo x observed, xc x calculated'''
    #fig, ax = plt.subplots()

    # ax.scatter(x,y)

    plt.plot(xc, yc, 'or', label='plt calc')
    plt.plot(xo, yo, '+b', label='plt obs')
    plt.plot(xd, yd, '+g', label='drone')
    plt.legend()
    plt.axis('equal')
    plt.show()


def platform_in_real_world_3D(xo, yo, zo, xc, yc, zc, xd, yd, zd, tc, dt):

    #axes.plot(xo, yo, zo, '+',c='b')
    fig = plt.figure()
    axes = fig.gca(projection='3d')
    axes.plot(xc, yc, zc, 'o', c='g')
    axes.plot(xd, yd, zd, 'o', c='magenta')
    for i in range(len(xc)):
        axes.text(xc[i], yc[i], zc[i], "{:.2f}".format(tc[i] - tc[0]))
    for i in range(len(xd)):
        axes.text(xd[i], yd[i], zd[i], "{:.2f}".format(dt*i))
    #print("xc ", len(xc))
    #print("xd ", len(xd))
    axes.set_xlabel('x')
    axes.set_ylabel('y')
    axes.set_zlabel('z')
    plt.show()


def kalman_in_3D(xpred, ypred, zpred, xmes, ymes, zmes, tc, dt):

    fig = plt.figure()
    axes = fig.gca(projection='3d')
    axes.plot(xmes, ymes, zmes, 'o', c='g', label='plt mes')
    axes.plot(xpred, ypred, zpred, 'o', c='magenta', label='plt pred')
    # print(len(tc))
    # print(len(xmes))
    for i in range(len(xmes)):
        axes.text(xmes[i], ymes[i], zmes[i], "{:.2f}".format(tc[i] - tc[0]))
    for i in range(len(xpred)):
        axes.text(xpred[i], ypred[i], zpred[i], "{:.2f}".format(dt*i))
    #print("xpred ", len(xpred))
    axes.set_xlabel('x')
    axes.set_ylabel('y')
    axes.set_zlabel('z')
    axes.legend()
    plt.show()


def kalman_in_time(xpred, ypred, zpred, xmes, ymes, zmes, tc, tpred):
    #tpred = [tc[0]+i*dt for i in range (len(xpred))]
    fig, axs = plt.subplots(3, figsize=(15, 8), dpi=150)
    axs[0].plot(tpred, xpred,  '.b', label='xpred')
    axs[0].plot(tc, xmes, '+r', label='xmes')
    axs[1].plot(tpred, ypred, '.b', label='ypred')
    axs[1].plot(tc, ymes, '+r', label='ymes')
    axs[2].plot(tpred, zpred, '.b', label='zpred')
    axs[2].plot(tc, zmes, '+r', label='zmes')
    axs[0].legend()
    axs[1].legend()
    axs[2].legend()
    plt.show()


def kalman_in_time_real_pos(xreal, yreal, zreal, xpred, ypred, zpred, xmes, ymes, zmes, tc, tpred, ts):
    fig, axs = plt.subplots(3, figsize=(15, 8), dpi=150)
    fig.suptitle("Positions de la plateforme réelle (OptiTrack), mesurée par la caméra et prédite par le filtre de Kalman", fontsize=16)
    axs[0].plot(tpred, xpred,  '.b', label='prédiction')
    axs[0].plot(tc, xmes, '+r', label='caméra')
    axs[0].plot(ts, xreal, '.g', label='OptiTrack')
    axs[1].plot(tpred, ypred, '.b', label='prédiction')
    axs[1].plot(tc, ymes, '+r', label='caméra')
    axs[1].plot(ts, yreal, '.g', label='OptiTrack')
    axs[2].plot(tpred, zpred, '.b', label='prédiction')
    axs[2].plot(tc, zmes, '+r', label='caméra')
    axs[2].plot(ts, zreal, '.g', label='OptiTrack')
    for i in range(3):
        axs[i].legend()
        axs[i].set_xlabel("temps (s)")
    axs[0].set_ylabel("x (m)")
    axs[1].set_ylabel("y (m)")
    axs[2].set_ylabel("z (m)")
    plt.show()


def calcul_ecart(x1, y1, z1, t1, x2, y2, z2, t2, step=0.1):

    def interpol(t, x, y, z, t0, tf, dt):
        fx = interp1d(t, x, kind='cubic')
        fy = interp1d(t, y, kind='cubic')
        fz = interp1d(t, z, kind='cubic')
        tnew = np.arange(t0, tf, dt)
        xnew = []
        ynew = []
        znew = []
        for tn in tnew:
            xnew.append(fx(tn))
            ynew.append(fy(tn))
            znew.append(fz(tn))
        return np.array(tnew), np.array(xnew), np.array(ynew), np.array(znew)
    t_start = max(t1[0], t2[0])
    t_end = min(t1[-1], t2[-1])
    t_new, x1_new, y1_new, z1_new = interpol(
        t1, x1, y1, z1, t_start, t_end, step)
    t_new, x2_new, y2_new, z2_new = interpol(
        t2, x2, y2, z2, t_start, t_end, step)
    return t_new, x1_new-x2_new, y1_new-y2_new, z1_new-z2_new


def ecart(xreal, yreal, zreal, treal, xpred, ypred, zpred, tpred, xcam, ycam, zcam, tcam):
    t_pred_real, dx_pred_real, dy_pred_real, dz_pred_real = calcul_ecart(
        xpred, ypred, zpred, tpred, xreal, yreal, zreal, treal, 0.1)
    t_pred_cam, dx_pred_cam, dy_pred_cam, dz_pred_cam = calcul_ecart(
        xpred, ypred, zpred, tpred, xcam, ycam, zcam, tcam, 0.1)
    fig, axs = plt.subplots(3, figsize=(15, 8), dpi=150)
    fig.suptitle("Écarts entre la position prédite par le filtre de Kalman et\n la position réelle (OptiTrack) ou la position mesurée par la caméra", fontsize=16)
    axs[0].plot(t_pred_real, dx_pred_real,  '.b', label="prédiction / OptiTrack")
    axs[0].plot(t_pred_cam, dx_pred_cam, '+r', label="prédiction / caméra")
    axs[1].plot(t_pred_real, dy_pred_real, '.b', label="prédiction / OptiTrack")
    axs[1].plot(t_pred_cam, dy_pred_cam, '+r', label="prédiction / caméra")
    axs[2].plot(t_pred_real, dz_pred_real, '.b', label="prédiction / OptiTrack")
    axs[2].plot(t_pred_cam, dz_pred_cam, '+r', label="prédiction / caméra")
    for i in range(3):
        axs[i].legend()
        axs[i].set_xlabel("temps (s)")
    axs[0].set_ylabel("x (m)")
    axs[1].set_ylabel("y (m)")
    axs[2].set_ylabel("z (m)")
    plt.show()


def angles(quaternions, t_img, quat_real, t_real):
    for i in range(len(t_img)):
        if len(t_img[i]) >= 4:
            x, y, z, x_real, y_real, z_real = [], [], [], [], [], []
            for j in range(len(t_img[i])):
                r = R.from_quat(quaternions[i][j])
                rot_euler = r.as_euler('xyz', degrees=True)
                x.append(rot_euler[1])
                y.append(rot_euler[0])
                # modification des angles car les quaternions ont été obtenu en NED alors qu'il fallait utiliser ENU
                z.append(-rot_euler[2])
            for k in range(len(t_real[i])):
                r_real = R.from_quat(quat_real[i][k])
                rot_euler_real = r_real.as_euler('xyz', degrees=True)
                x_real.append(rot_euler_real[0])
                y_real.append(rot_euler_real[1])
                z_real.append(rot_euler_real[2])
            fig, axs = plt.subplots(3, figsize=(15, 8), dpi=150)
            axs[0].plot(t_img[i], x,  '.b', label='angle caméra')
            axs[0].plot(t_real[i], x_real, '+r', label='angle OptiTrack')
            axs[1].plot(t_img[i], y, '.b', label='angle caméra')
            axs[1].plot(t_real[i], y_real, '+r', label='angle OptiTrack')
            axs[2].plot(t_img[i], z, '.b', label='angle caméra')
            axs[2].plot(t_real[i], z_real, '+r', label='angle OptiTrack')
            fig.suptitle("Angles d'Euler en fonction du temps", fontsize=16)
            for i in range(3):
                axs[i].legend()
                axs[i].set_xlabel("temps (s)")
            axs[0].set_ylabel("tangage (°)")
            axs[1].set_ylabel("roulis (°)") #à déterminer
            axs[2].set_ylabel("lacet (°)")
            # axs[0].legend()
            # axs[1].legend()
            # axs[2].legend()
            plt.show()
