from scipy.spatial.transform import Rotation as R
import numpy as np


def scale(x, y, z, aruco=0.154, factor=100):
    k = aruco/factor
    return k*x, k*y, k*z


def translate(pos_drone, pos_target_cam, dpos_cam):
    pos_target_x = pos_drone[0] + pos_target_cam[0]
    pos_target_y = pos_drone[1] + pos_target_cam[1]
    pos_target_z = pos_drone[2] + pos_target_cam[2]

    return pos_target_x, pos_target_y, pos_target_z


# Avec les quaternions de la centrale inertielle
def rotate_cam_to_bar(pos_target_cam, quaternion):

    r1 = R.from_quat(quaternion)
    rot_euler = r1.as_euler('xyz', degrees=True)
    # passage de NED à ENU
    rot_euler[0], rot_euler[1] = rot_euler[1], rot_euler[0]
    rot_euler[2] = - rot_euler[2]
    r = R.from_euler('xyz', rot_euler, degrees=True)

    rot_mat = r.as_matrix()

    pos_target_x = pos_target_cam[1]  # passage de cam à drone
    pos_target_y = pos_target_cam[0]
    pos_target_z = - pos_target_cam[2]

    # passage de drone à bar
    pos_target_cam = np.dot(
        rot_mat, [pos_target_x, pos_target_y, pos_target_z])

    return pos_target_cam[0], pos_target_cam[1], pos_target_cam[2]


# Avec les quaternions de la centrale inertielle
def rotate_drone_to_bar(pos_target_cam, quaternion):
    r1 = R.from_quat(quaternion)
    rot_euler = r1.as_euler('xyz', degrees=True)
    # passage de NED à ENU
    rot_euler[0], rot_euler[1] = rot_euler[1], rot_euler[0]
    rot_euler[2] = - rot_euler[2]
    r = R.from_euler('xyz', rot_euler, degrees=True)

    rot_mat = np.linalg.inv(r.as_matrix())

    pos_target_cam = np.dot(rot_mat, pos_target_cam)

    return pos_target_cam[0], pos_target_cam[1], pos_target_cam[2]


# Avec les quaternions de l'OptiTrack
def rotate_real_drone_to_bar(pos_target_cam, quaternion):
    r = R.from_quat(quaternion)
    rot_mat = np.linalg.inv(r.as_matrix())

    pos_target_cam = np.dot(rot_mat, pos_target_cam)

    return pos_target_cam[0], pos_target_cam[1], pos_target_cam[2]


def rotate_drone_to_cam(pos_target_cam):
    return (pos_target_cam[1], pos_target_cam[0], -pos_target_cam[2])


def pl_ter_to_pl_bar(xd, yd, zd, x_pl, y_pl, z_pl, quat_drone):
    x_pl_bar, y_pl_bar, z_pl_bar = [], [], []

    for i in range(min(len(xd), len(x_pl))):
        x_pl_bar.append(- xd[i] + x_pl[i])
        y_pl_bar.append(- yd[i] + y_pl[i])
        z_pl_bar.append(- zd[i] + z_pl[i])
    return x_pl_bar, y_pl_bar, z_pl_bar
