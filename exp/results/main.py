#!/usr/bin/python

import reader as rd
import frame as fr
import trace as tr
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import csv
import kalman as k

import matplotlib.pyplot as plt
filename_log = "exp_01.LOG"

t_img, x_drone_img, y_drone_img, z_drone_img, xc, yc, zc, quaternions = rd.read_log(
    filename_log)

filename_measures = "exp_01_measures"

ts, x_drone_218, y_drone_218, z_drone_218, quaternions_real, x_pl, y_pl, z_pl = rd.read_measures(
    filename_measures, t_img)

xc, yc, zc = fr.scale(xc, yc, zc)

pos_target_x, pos_target_y, pos_target_z = np.matrix.copy(
    xc), np.matrix.copy(yc), np.matrix.copy(zc)


# for line in range(len(xc)):

#     rot1_eulers = []
#     rot_eulers = []
#     for column in range(len(xc[line])):
#         pos_drone = np.array([x_drone_img[line][column], y_drone_img[line][column], z_drone_img[line][column]])
#         pos_target_cam = np.array([xc[line][column], yc[line][column], zc[line][column]])
#         quaternion = quaternions[line][column]
#         dpos_cam = np.array([0, 0, 0])
#         pos_target_x[line][column], pos_target_y[line][column], pos_target_z[line][column] = fr.translate(pos_drone, fr.rotate(pos_target_cam, quaternion), dpos_cam)

# tr.platform_in_real_world(x_pl[line], y_pl[line], z_pl[line], pos_target_x[line], pos_target_y[line], pos_target_z[line], x_drone_img[line], y_drone_img[line], 0)
# tr.platform_in_real_world_3D(x_pl[line], y_pl[line], z_pl[line], pos_target_x[line], pos_target_y[line], pos_target_z[line], x_drone_img[line], y_drone_img[line], z_drone_img[line])


# tr.angles(quaternions, t_img, quaternions_real, ts)

k.courbes(xc, yc, zc, quaternions, t_img, x_drone_218, y_drone_218,
          z_drone_218, x_pl, y_pl, z_pl, quaternions_real, ts)

# k.test_opt_kalman(xc, yc, zc, quaternions, t_img, x_drone_218,
#                  y_drone_218, z_drone_218, x_pl, y_pl, z_pl, quaternions_real, ts)
