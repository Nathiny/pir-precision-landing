from filterpy.kalman import KalmanFilter
import numpy as np
import trace as tr
import frame as fr
from filterpy.common import Q_discrete_white_noise
import scipy.optimize as opt


VCONS, RCONS = 35e-6, 1e-1


def kalman(X, dt=0.1, var=0.13, r_factor=1):

    f = KalmanFilter(dim_x=6, dim_z=3)

    f.x = np.array(X[0][0:-1])

    size = 6
    F = np.zeros((size, size))
    for i in range(size):
        if i % 2 == 0:
            F[i, i] = 1
            F[i, i+1] = dt
        elif i % 2 == 1:
            F[i, i] = 1

    # X(k) = FX(k-1)
    print("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFf \n", F)

    f.F = F

    H = np.zeros((3, size))
    H[0, 0], H[1, 2], H[2, 4] = 1, 1, 1

    f.H = H

    print("H \n", H)

    f.P = 1000*np.eye(size)

    f.R = r_factor*np.eye(3)  # 5*np.eye(3)

    f.Q = Q_discrete_white_noise(2, dt=0.1, var=var, block_size=3)
    print("Q   ", f.Q)

    xs = []
    i = 1
    #print(X[0][-1], X[-1][-1])
    t_pred = np.arange(X[0][-1], X[-1][-1] + 20*dt, dt)
    for t in t_pred:
        while i < len(X) and t > X[i][-1]:
            f.update([X[i][k] for k in range(0, 5, 2)])
            i += 1
        f.predict()
        xs.append(f.x)
        # print(len(xs))
    return xs, t_pred


def courbes(xc, yc, zc, quaternions, t_img, xd, yd, zd, x_pl, y_pl, z_pl, quat_drone, t_real):
    for line in range(len(xc)):
        # position de la plateforme dans le repère barycentrique donnée par la caméra
        x_rotates, y_rotates, z_rotates = [], [], []
        if len(xc[line]) >= 4:

            x_pl_bar, y_pl_bar, z_pl_bar = fr.pl_ter_to_pl_bar(
                xd[line], yd[line], zd[line], x_pl[line], y_pl[line], z_pl[line], quat_drone[line])

            for column in range(len(xc[line])):
                pos_target_cam = np.array(
                    [xc[line][column], yc[line][column], zc[line][column]])
                quaternion = quaternions[line][column]
                xtemp, ytemp, ztemp = fr.rotate_cam_to_bar(
                    pos_target_cam, quaternion)
                x_rotates.append(xtemp)
                y_rotates.append(ytemp)
                z_rotates.append(ztemp)
            print(len(xc[line]))
            print(len(x_rotates))
            x_init = np.array([x_rotates[0], 0, y_rotates[0],
                               0, z_rotates[0], 0, t_img[line][0]])
            x = np.array([[x_rotates[i],
                           (x_rotates[i+1] - x_rotates[i-1]) /
                           (t_img[line][i+1] - t_img[line][i-1]),
                           y_rotates[i],
                           (y_rotates[i+1] - y_rotates[i-1]) /
                           (t_img[line][i+1] - t_img[line][i-1]),
                           z_rotates[i],
                           (z_rotates[i+1] - z_rotates[i-1]) /
                           (t_img[line][i+1] - t_img[line][i-1]),
                           t_img[line][i]] for i in range(1, len(x_rotates)-1)])

            print(x_init)

            x = np.vstack((x_init, x))
            # print("x : ", x)
            dt = 0.1

            #var, r_factor = 0.13, 1e3
            # var, r_factor = 1e1, 1e-3
            var, r_factor = VCONS, RCONS

            Xs, ts = kalman(x, dt, var, r_factor)
            #print("lenXS", len(Xs))
            xs = [Xs[i][0] for i in range(len(Xs))]
            ys = [Xs[i][2] for i in range(len(Xs))]
            zs = [Xs[i][4] for i in range(len(Xs))]
            print("l xs :", len(xs))
            print("l quat : ", len(quaternions[line]))
            #tr.kalman_in_3D(xs, ys, zs, x_rotates, y_rotates, z_rotates, t_img[line], dt)
            #tr.kalman_in_time(xs, ys, zs, x_rotates, y_rotates, z_rotates, t_img[line], ts)
            # tr.kalman_in_time_real_pos(x_pl_bar, y_pl_bar, z_pl_bar, xs, ys, zs,                                        x_rotates, y_rotates, z_rotates, t_img[line], ts, t_real[line])
            tr.ecart(x_pl_bar, y_pl_bar, z_pl_bar, t_real[line], xs, ys, zs, ts, x_rotates, y_rotates, z_rotates, t_img[line])


def optimize_kalman_parameters(xs_ter, ys_ter, zs_ter, ts_ter, X, params0=(0.13, 1)):
    """params0 : starting tuple (var, r_factor) for the research"""
    # plt.plot(ts, xs)
    # popt, pcov = curve_fit(kalman, ts, xpred)
    # plt.plot(ts, func(xs, *popt))

    dt = 0.1

    def func(params):
        var, r_factor = params[0]/1e7, params[1]*1e3
        # var, r_factor = params[0], params[1]
        # print("var : ", var)
        # print("r : ", r_factor)
        X_pred, t_pred = kalman(X, dt, var, r_factor)

        x_pred = [X_pred[i][0] for i in range(len(X_pred))]
        y_pred = [X_pred[i][2] for i in range(len(X_pred))]
        z_pred = [X_pred[i][4] for i in range(len(X_pred))]

        t_new, dxs, dys, dzs = tr.calcul_ecart(
            xs_ter, ys_ter, zs_ter, ts_ter, x_pred, y_pred, z_pred, t_pred, step=0.1)
        d_square_sum = 0
        for i in range(len(dxs)):
            d_square_sum += dxs[i]**2 + dys[i]**2 + dzs[i]**2
        # print("d: ", d_square_sum, var, r_factor )
        return d_square_sum

    def manual_func(bounds_var, bounds_r):
        best_var = -1
        best_r = -1
        best_square = float('inf')
        for i, var in enumerate(np.linspace(bounds_var[0], bounds_var[1], 100)):
            for r in np.linspace(bounds_r[0], bounds_r[1], 100):
                d_square_sum = func((var, r))
                if best_square > d_square_sum:
                    best_r = r
                    best_var = var
                    best_square = d_square_sum
                    print("best square change")
            print(i)
        return r, var

    # return manual_func((1e-7, 1e-4), (1e3, 1e5))
    return opt.minimize(func, params0, method="L-BFGS-B", bounds=((1, 100), (1, 100)), options={'eps': 1})


def test_opt_kalman(xc, yc, zc, quaternions, t_img, xd, yd, zd, x_pl, y_pl, z_pl, quat_drone, t_real):
    for line in range(len(xc)):
        x_rotates, y_rotates, z_rotates = [], [], []
        if len(xc[line]) >= 3:
            xs_ter, ys_ter, zs_ter = fr.pl_ter_to_pl_bar(
                xd[line], yd[line], zd[line], x_pl[line], y_pl[line], z_pl[line], quat_drone[line])
            ts_ter = t_real[line]

            # Get initial X
            for column in range(len(xc[line])):
                pos_target_cam = np.array(
                    [xc[line][column], yc[line][column], zc[line][column]])
                quaternion = quaternions[line][column]
                xtemp, ytemp, ztemp = fr.rotate_cam_to_bar(
                    pos_target_cam, quaternion)
                x_rotates.append(xtemp)
                y_rotates.append(ytemp)
                z_rotates.append(ztemp)
            # print(len(xc[line]))
            # print(len(x_rotates))
            x_init = np.array([x_rotates[0], 0, y_rotates[0],
                               0, z_rotates[0], 0, t_img[line][0]])
            x = np.array([[x_rotates[i],
                           (x_rotates[i+1] - x_rotates[i-1]) /
                           (t_img[line][i+1] - t_img[line][i-1]),
                           y_rotates[i],
                           (y_rotates[i+1] - y_rotates[i-1]) /
                           (t_img[line][i+1] - t_img[line][i-1]),
                           z_rotates[i],
                           (z_rotates[i+1] - z_rotates[i-1]) /
                           (t_img[line][i+1] - t_img[line][i-1]),
                           t_img[line][i]] for i in range(1, len(x_rotates)-1)])
            X = np.vstack((x_init, x))

            # Optimizing kalman paramaters with opt.minimize
            res = optimize_kalman_parameters(xs_ter, ys_ter, zs_ter, ts_ter, X)
            print("res :", res.x)

            # Optimizing kalman manually
            # res = optimize_kalman_parameters(xs_ter, ys_ter, zs_ter, ts_ter, X)
            # print("res :", res)
