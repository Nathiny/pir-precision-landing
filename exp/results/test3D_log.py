from numpy import *
from matplotlib.pyplot import *
from mpl_toolkits.mplot3d import Axes3D
import csv
filename="exp_01.LOG"
file=open(filename,"r")
x_drone, y_drone, z_drone = [],[],[]
x_drone_img, y_drone_img, z_drone_img = [],[],[]
t_img = []
i = 0
for row in file :
    i += 1
    line=row.split(" ")
    if i>1 :
        #if line[1] != 'None':
        x_drone.append(float(line[13]))
        y_drone.append(float(line[14]))
        z_drone.append(float(line[15]))

        if line[1] != 'None':
            x_drone_img.append(float(line[13]))
            y_drone_img.append(float(line[14]))
            z_drone_img.append(float(line[15]))
            t_img.append(float(line[-1]))

filename="exp_01_measures"
file=open(filename,"r")
x_drone_218, y_drone_218, z_drone_218 = [],[],[]
x_pl, y_pl, z_pl = [], [], []
i = 0
for row in file :
    i += 1
    line=row.split()
    if i>1 :
        if line[0] == '218':
            x_drone_218.append(float(line[1]))
            y_drone_218.append(float(line[2]))
            z_drone_218.append(float(line[3]))
        if line[0] == '51':
            x_pl.append(float(line[1]))
            y_pl.append(float(line[2]))
            z_pl.append(float(line[3]))


file.close()
fig = figure()
axes = fig.gca(projection='3d')
axes.plot(x_drone, y_drone, z_drone, c='b')
axes.plot(x_drone_img, y_drone_img, z_drone_img, '+', c='black')
axes.plot(x_drone_218, y_drone_218, z_drone_218, c='magenta')
axes.plot(x_pl, y_pl, z_pl, c='g')
axes.set_xlabel('x')
axes.set_ylabel('y')
axes.set_zlabel('z')


show()