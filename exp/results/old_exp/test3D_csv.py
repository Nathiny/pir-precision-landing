from numpy import *
from matplotlib.pyplot import *
from mpl_toolkits.mplot3d import Axes3D
import csv
filename = "test.csv"
file=open(filename,"r")
x_drone, y_drone, z_drone = [],[],[]
x_plateform, y_plateform, z_plateform = [],[],[]
i = 0
for row in file :
    i += 1
    line=row.split(",")
    if i>8 :
        x_drone.append(float(line[6]))
        y_drone.append(float(line[7]))
        z_drone.append(float(line[8]))
        if line[13]!="" :
            x_plateform.append(float(line[13]))
            y_plateform.append(float(line[14]))
            z_plateform.append(float(line[15]))            

file.close()
gca(projection='3d').plot(x_drone ,z_drone, y_drone)
gca(projection='3d').plot(x_plateform, z_plateform, y_plateform)
show()