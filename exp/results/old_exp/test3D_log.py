from numpy import *
from matplotlib.pyplot import *
from mpl_toolkits.mplot3d import Axes3D
import csv
filename="exp_01.LOG"
file=open(filename,"r")
x_drone, y_drone, z_drone = [],[],[]
i = 0
for row in file :
    i += 1
    line=row.split(" ")
    if i>1 :
        #if line[1] != 'None':
        x_drone.append(float(line[13]))
        y_drone.append(float(line[14]))
        z_drone.append(float(line[15]))
file.close()
gca(projection='3d').plot(x_drone, z_drone, y_drone)
show()