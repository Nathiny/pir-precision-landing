import numpy as np
from typing import Dict, Tuple
import sys
import os

sys.path.append(os.environ["PAPARAZZI_HOME"] + '/sw/ext/pprzlink/lib/v2.0/python')

from ivy.std_api import *
import pprzlink.ivy

import pprzlink.messages_xml_map as messages_xml_map
import pprzlink.message as message

SAMPLING_STEP = 10

DEFAULT_BUS = '127.0.0.1:2010'
GPS_REGEX = "(.*GROUND_REF.*)"
f = "measures" 

class Measurer():

    def __init__(self, parent=None):
        # Creation of the ivy interface
        self.ivy = pprzlink.ivy.IvyMessagesInterface(
            agent_name="Ostricht",                # Ivy agent name
            start_ivy=False,                    # Do not start the ivy bus now
            ivy_bus=DEFAULT_BUS)     # address of the ivy bus
        self.ivy.start()
        self.ivy.subscribe(self.update_gps, message.PprzMessage("telemetry", "GROUND_REF"))
        
    def closing(self):
        self.ivy.shutdown()
        pass

    def update_gps(self, ac_id, pprzMsg):
        x = float(pprzMsg.pos_x)
        y = float(pprzMsg.pos_y)
        flight_time = float(pprzMsg.flight_time)
        block_time = float(pprzMsg.block_time)
        stage_time = float(pprzMsg.stage_time)
        with open(f, 'a') as g:
            g.write("{:d} \t {:6f} \t {:6f} \t {:6f} \t {:6f} \t {:6f}\n".format(ac_id, x, y, flight_time, block_time, stage_time))

measurer = Measurer()