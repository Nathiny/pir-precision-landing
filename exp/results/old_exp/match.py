from numpy import *

filename="Take 2020-02-07 02.51.22 PM.csv"
file=open(filename,"r")
xcsv,ycsv,zcsv,tcsv = [],[],[], []
i = 0
for row in file :
    i += 1
    line=row.split(",")
    if i>8 :
        xcsv.append(float(line[6]))
        ycsv.append(float(line[7]))
        zcsv.append(float(line[8]))
        tcsv.append(float(line[1]))

filename="pprzlog_0008.LOG"
file=open(filename,"r")
xlog,ylog,zlog,tlog = [],[],[], []
i = 0
for row in file :
    i += 1
    line=row.split(" ")
    if i>1 :
        xlog.append(float(line[13]))
        ylog.append(float(line[15]))
        zlog.append(float(line[14]))
        tlog.append(float(line[-1]))


c = 0
eps = 0.001
nlog = len(xlog)
ncsv = len(xcsv)
jinit = 0
tc = 0
tl = 0
for i in range(ncsv):
    xc = xcsv[i]
    yc = ycsv[i]
    zc = zcsv[i]
    for j in range(jinit, nlog):  
        xl = xlog[j]
        yl = ylog[j]
        zl = zlog[j]    
        if abs(xl - xc) < 5*eps and abs(yl - yc) < 1 and abs(zl + zc) < 5*eps:
            print("(dtcsv,dtlog) = (",tcsv[i]-tc,",",tlog[j]-tl,")")
            print("(i,j) = (",i,",",j,")")
            print("(xc,yc,zc) = (",xc,",",yc,",",zc,")")
            print("(xl,yl,zl) = (",xl,",",yl,",",zl,")\n")
            c += 1
            jinit = j + 1
            tc = tcsv[i]
            tl = tlog[j]
            break

print(c)

