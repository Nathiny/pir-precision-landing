#!/usr/bin/python

import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import csv

aruco = 0.154 # m
cam_drone = 0.065 # m


def scale(x, y, z, aruco):
    k = aruco/100
    return k*x, k*y, k*z

filename="exp_01.LOG"
file=open(filename,"r")
x_drone, y_drone, z_drone = [],[],[]
x_drone_img, y_drone_img, z_drone_img = [],[],[]
t_img = []
xc, yc, zc = [], [], []
i = 0
s = 0
eps = 2 #seconds
for row in file :
    i += 1
    line=row.split(" ")
    if i==2 :
        x_drone_img.append([float(line[13])])
        y_drone_img.append([float(line[14])])
        z_drone_img.append([float(line[15])])
        xc.append([float(line[3])])
        yc.append([float(line[4])])
        zc.append([float(line[5])])
        t_img.append([float(line[-1])/1000])

    if i>2 :
        if line[1] != 'None':
            t = float(line[-1])/1000
            if t - t_img[s][-1] > eps:
                s += 1
                x_drone_img.append([float(line[13])])
                y_drone_img.append([float(line[14])])
                z_drone_img.append([float(line[15])])
                t_img.append([float(line[-1])/1000])
                xc.append([float(line[3])])
                yc.append([float(line[4])])
                zc.append([float(line[5])])
            elif t != t_img[s][-1] :
                x_drone_img[s].append(float(line[13]))
                y_drone_img[s].append(float(line[14]))
                z_drone_img[s].append(float(line[15]))
                t_img[s].append(float(line[-1])/1000)
                x, y, z = float(line[3]), float(line[4]), float(line[5])
                xc[s].append(x)
                yc[s].append(y)
                zc[s].append(z)

file.close() 

filename="exp_01_measures"
file=open(filename,"r")
x_drone_218, y_drone_218, z_drone_218 = [[]],[[]],[[]]
x_pl, y_pl, z_pl = [[]], [[]], [[]]
i = 0
s = 0
for row in file :
    i += 1
    line=row.split()
    t = float(line[-1])
    if s < len(t_img) and t >= t_img[s][0] and t <= t_img[s][-1] :
        if line[0] == '218':
            x_drone_218[s].append(float(line[1]))
            y_drone_218[s].append(float(line[2]))
            z_drone_218[s].append(float(line[3]))
        if line[0] == '51':
            x_pl[s].append(float(line[1]))
            y_pl[s].append(float(line[2]))
            z_pl[s].append(float(line[3]))
    if s < len(t_img) and t > t_img[s][-1]:
        s += 1
        x_drone_218.append([])
        y_drone_218.append([])
        z_drone_218.append([])
        x_pl.append([])
        y_pl.append([])
        z_pl.append([])


for session in range(len(xc)):
    xs = xc[session]
    ys = yc[session]
    zs = zc[session]
    ts = t_img[session]

    vxs = [0]
    vys = [0]
    vzs = [0]
    dt = [0]

    nb_moy = 3

    ts.append(ts[-1] + 0.2)
    print("ts", ts)
    for i in range(1, len(xs)):
        dt.append(ts[i] - ts[i-1])
        vxs.append((xs[i] - xs[i-1])/dt[i])
        vys.append((ys[i] - ys[i-1])/dt[i])
        vzs.append((zs[i] - zs[i-1])/dt[i])
        
    dt.append(ts[-1] - ts[-2])

    dx = [0 for _ in range(nb_moy)]

    for k in range(nb_moy, len(vxs)+1):
        num = sum([vxs[i] * dt[i] for i in range(k-nb_moy+1,k)])
        den = sum([dt[i] for i in range(k-nb_moy+1,k)])
        vxmoy = num/den
        dx.append(vxmoy * dt[k])

    dy = [0 for _ in range(nb_moy)]

    for k in range(nb_moy, len(vys)+1):
        num = sum([vys[i] * dt[i] for i in range(k-nb_moy+1,k)])
        den = sum([dt[i] for i in range(k-nb_moy+1,k)])
        vymoy = num/den
        dy.append(vymoy * dt[k])

    dz = [0 for _ in range(nb_moy)]

    for k in range(nb_moy, len(vzs)+1):
        num = sum([vzs[i] * dt[i] for i in range(k-nb_moy+1,k)])
        den = sum([dt[i] for i in range(k-nb_moy+1,k)])
        vzmoy = num/den
        dz.append(vzmoy * dt[k])

    xs_est = [xs[i-1] + dx[i] if i >= nb_moy else 0 for i in range(len(dx))]
    ys_est = [ys[i-1] + dy[i] if i >= nb_moy else 0 for i in range(len(dy))]
    zs_est = [zs[i-1] + dz[i] if i >= nb_moy else 0 for i in range(len(dz))]

    """
    print('\n\n\n')

    print('vxs :', vxs)
    print('ts :', ts)
    print('xs : ', xs)
    print('xs_est: ', xs_est)
    print('ys : ', ys)
    print('ys_est : ', ys_est)
    print('zs : ', zs)
    print('zs_est : ', zs_est)
    """

    fig, (ax1,ax2) = plt.subplots(2)

    ax1.scatter(xs,ys)

    for i in range(len(xs)):
        ax1.annotate(i, (xs[i], ys[i]))

    xs_est_calc = xs_est[(nb_moy):len(xs)+1]
    ys_est_calc = ys_est[(nb_moy):len(ys)+1]

    ax1.scatter(xs_est_calc,ys_est_calc)

    for i in range(len(xs_est_calc)):
        ax1.annotate(i+nb_moy, (xs_est_calc[i], ys_est_calc[i]))


    print("\n\n\n\n x_d", k, "xd, ", x_drone_218)
    print("\n\n",y_drone_218)
    ax2.scatter(x_drone_218[session],y_drone_218[session])
    ax2.scatter(x_pl[session],y_pl[session])
    ax2.axis('equal')

    plt.show()


